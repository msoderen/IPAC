\documentclass{beamer}
\usepackage[orientation=portrait, size=a0]{beamerposter}
\usepackage{tcolorbox}
\usepackage{multicol}
\usepackage{float}
\usepackage[utf8]{inputenc}
\usepackage[]{algorithm2e}
\usepackage{caption,subcaption}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{makecell}
\usepackage{setspace}
\usepackage{wrapfig}
\setlength{\multicolsep}{1.0pt plus 4.0pt minus 5.7pt}%
\setlength\columnsep{20mm}
\setbeamersize{text margin left=20mm,text margin right=20mm} 
\setbeamertemplate{navigation symbols}{}%remove navigation symbols
\definecolor{cernBlue}{RGB}{0,83,161}
\definecolor{backgroundColor}{RGB}{0,0,80}
\setbeamertemplate{caption}[numbered]

\addtolength{\headsep}{20mm}
\addtolength{\paperheight}{20mm}

\begin{document}
%title
\begin{tcolorbox}[colback=white,colframe=black,colbacktitle=cernBlue,sharp corners,boxrule=0.5pt,text width=\columnwidth,boxsep=-2mm,left=2mm,right=0mm,before
skip=0pt plus 0pt]
       \begin{minipage}[t]{0.075\textwidth}
    \vspace*{0pt}
\includegraphics[width=70mm]{LogoBadge.eps}
    \end{minipage}
  \begin{minipage}[t]{0.915\textwidth}
    \vspace*{10mm}
{\centering
    {\fontsize{80}{40}\selectfont Online bunch by bunch transverse instability detection in LHC}
    \par}
    \vspace*{10mm}
    {\centering
        {\fontsize{35}{30}\selectfont M.E Söderén, G. Kotzian, M. Ojeda-Sandonís, D. Valuch,CERN, Geneva, Switzerland}
    \par}
    \end{minipage}
\end{tcolorbox}
\vspace*{5mm}
%abstract
\begin{tcolorbox}[colback=cernBlue,colframe=black,title=Abstract,colbacktitle=cernBlue,sharp
 corners,boxrule=0.5pt,standard jigsaw,opacityback=0.2,text width=0.985\textwidth]
\fontsize{30}{30}\selectfont
Reliable detection of developing transverse instabilities in the Large Hadron Collider is one of the main operational challenges of the LHC's high intensity proton run. A full machine snapshot provided from the moment of instability is a crucial input to develop and fine tune instability models. The transverse feedback system (ADT) is the only instrument in LHC, where a full rate bunch by bunch transverse position information is available. Together with a sub-micron resolution it makes it a perfect place to detect transverse beam motion. Very large amounts of data, at very high data rates (8 Gb/s) need to be processed on the fly to detect onset of transverse instability. A very powerful computer system (so called ADTObsBox) was developed and put into operation by the CERN RF group, which is capable of processing the full rate data streams from ADT and perform an on the fly instability detection. The output of this system is a timing event with a list of all bunches developing instability, which is then sent to the LHC-wide instability trigger network to freeze other observation instruments. The device also provides buffers with raw position data for offline analysis.
\end{tcolorbox}
%first multicols
\begin{multicols}{2}
%obsbox
\begin{tcolorbox}[colback=cernBlue,colframe=black,title=Bunch by bunch transverse observtion system,colbacktitle=cernBlue,sharp corners,boxrule=0.5pt,standard jigsaw,opacityback=0.2,text width=0.48\textwidth,boxsep=5mm,height=190mm]
    \begin{minipage}[t]{0.5\textwidth}
    \vspace*{10pt}
\includegraphics[width=\textwidth]{sketch.eps}
\end{minipage}
\hspace{1mm}
    \begin{minipage}[t]{0.5\textwidth}
    \vspace*{0pt}
    \fontsize{30}{30}\selectfont
    Since the ADTObsBox observation system had been commissioned in 2015, it is used for injection oscillation transient analysis, with online and offline beam parameter and transverse feedback parameter extraction. The system is also an invaluable resource for the machine development studies, as it is the only place in the LHC machine where a bunch-by-bunch transverse position data is available for unlimited number of turns. The positional data are available to all users or other systems in a form of data arrays through a subscription to the ObsBoxBuffer FESA class. Now it is also host for the LHC instability analysis.
    \end{minipage}
\end{tcolorbox}
\vspace*{-50mm}
%Detection algorithm
\begin{tcolorbox}[colback=cernBlue,colframe=black,title=Transverse instability detection algorithm,colbacktitle=cernBlue,sharp corners,boxrule=0.5pt,standard jigsaw,opacityback=0.2,text width=0.48\textwidth,boxsep=5mm,height=290mm]
       \begin{minipage}[t]{0.50\textwidth}
    \vspace*{0pt}
\resizebox{!}{0.65\textwidth}{
\begin{algorithm}[H]
 \KwData{Oscillation Amplitude($A[n]$)}
 \KwResult{instability trigger}
 \tcc{Prefill $avg$ and $avg_{old}$ with samples}
 $avg=\sum\limits_{n=0}\limits^{W}\dfrac{A[n]}{W}$\;
 $thres=avg$\;
 \tcc{Do this for every amplitude sample}
 \While{forever}{
 \For{W times}{
 \tcc{update average and remove the oldest sample}
 $avg+=new \; \dfrac{A}{W}$\;
 $avg-=oldest \;\dfrac{A}{W}$\;
 }
 \tcc{$\omega$ adjusts the sensitivity}
   \If{$thres<\omega\times avg$}{
   send trigger\;
   }
   \tcc{$\beta$ can be set to limit the growth rate of the threshold}
   $thres$=$\beta\times avg+(1-\beta)\times thres$\;
 }
 \label{alg:alg}
\end{algorithm}
}
    \end{minipage}
    \hspace{1mm}
  \begin{minipage}[t]{0.5\textwidth}
    \vspace*{2pt}
    \fontsize{30}{30}\selectfont
    The system has to be able to detect a large variety of
instability signatures. These include very different rise times
(typically from 100’s turns, up to 100k’s turns) or a very different activity behavior. At the same time, the system should
produce as few false triggers as possible. From experience moving averages was chosen as the detection method.
The algorithm uses three moving averages, each with subsequently longer integration window. Use of multiple windows with different lengths allows for reliable detection of activities with very different rise times. The algorithm compares the value of the current window (the average of the last $W$ samples) against a threshold, which is calculated as an accumulated sum of the previous windows. The detection sensitivity, efficiency and immunity to perturbations can be adjusted by careful selection of the $\beta$ and $\omega$ parameters. 
    \end{minipage}\hfill%
\end{tcolorbox}
\vspace*{-50mm}
%Pipeline
\begin{tcolorbox}[colback=cernBlue,colframe=black,title=Computation pipeline,colbacktitle=cernBlue,sharp corners,boxrule=0.5pt,standard jigsaw,opacityback=0.2,text width=0.48\textwidth,boxsep=5mm,height=460mm]
\begin{figure}
\includegraphics[width=\textwidth,height=220mm]{test.eps}
\caption{Each stage in the pipeline with real beam data}
\label{fig:pipeline}
\end{figure}

        \begin{minipage}[t]{0.5\textwidth}
        \begin{figure}
 \vspace*{0pt}
          \resizebox {\columnwidth} {170mm} {
   \fontsize{20}{10}
\input{pipeline.tex}
}
\end{figure}
\end{minipage}
 \begin{minipage}[t]{0.5\textwidth}
\vspace*{5pt}
\fontsize{30}{30}\selectfont
The normalized bunch transverse position is received by the ADTObsBox in a form of 1 GBit/s stream of 16-bit integer data (per pickup). The data is sliced turn-by-turn, converted to float and memory-aligned with 32 bit for AVX2 compatibility(see Input in figure~\ref{fig:pipeline}). The closed orbit offset is removed by a two tap notch filter(see notch in figure~\ref{fig:pipeline}) and the bunch oscillation amplitude is calculated from the position data using the Hilbert transformer(see I and Q in figure~\ref{fig:pipeline}). The oscillation amplitude(see Amplitude in figure~\ref{fig:pipeline}) is calculated from the I and Q components and passed to the instability detection algorithm. The output from the detection algorithm is visible in Window[254],Window[1024] and window[4096] in figure~\ref{fig:pipeline}. 
   \end{minipage}
\end{tcolorbox}

%testing
\begin{tcolorbox}[colback=cernBlue,colframe=black,title=Algorithm testing with real beam data,colbacktitle=cernBlue,sharp corners,boxrule=0.5pt,standard jigsaw,opacityback=0.2,text width=0.463\textwidth,boxsep=5mm,height=520mm]
    \begin{minipage}[t]{0.65\textwidth}
    \vspace*{0pt}
    \begin{figure}
    \includegraphics[width=0.98\textwidth]{slow.png}
    \end{figure}
    \end{minipage}
    \vspace*{10pt}
        \begin{minipage}[t]{0.34\textwidth}
    	\vspace*{0pt}
    	\fontsize{30}{30}\selectfont
		After injection a slow amplitude increase occurred and the longest integration window detected it. Afterwards an fast exponential amplitude growth occurred and all windows detected it.
    \end{minipage}
    \vspace*{10pt}
        \begin{minipage}[t]{0.65\textwidth}
    \vspace*{5pt}
    \begin{figure}
    \includegraphics[width=0.98\textwidth]{normal.png}
    \end{figure}
    \end{minipage}
        \begin{minipage}[t]{0.34\textwidth}
    \vspace*{5pt}
    \fontsize{30}{30}\selectfont
20000 turns after injection an exponential amplitude growth took place and was discovered by all windows. This is a schoolbook example of a transverse oscillation instability.
    \end{minipage}
      \vspace*{10pt}
    \begin{minipage}[t]{0.65\textwidth}
    \vspace*{5pt}
    \begin{figure}
    \includegraphics[width=0.98\textwidth]{after_injection.png}
    \end{figure}
    \end{minipage}
        \begin{minipage}[t]{0.34\textwidth}
    \vspace*{5pt}
    \fontsize{30}{30}\selectfont
A small instability developed directly after injection and was detected by all windows. It was followed by a slower increase that was only detected by the longest window.
    \end{minipage}
    
        \begin{minipage}[t]{0.65\textwidth}
    \vspace*{5pt}
    \begin{figure}
    \includegraphics[width=0.98\textwidth]{jumping.png}
    \end{figure}
    \end{minipage}
        \begin{minipage}[t]{0.34\textwidth}
    \vspace*{5pt}
    \fontsize{30}{30}\selectfont
Instability with both lower and higher frequency components. The higher frequencies are easily detected by the shortest window and the lower frequencies are detected by all windows.
    \end{minipage}
\end{tcolorbox}
\vspace*{8mm}
%improvement
\begin{tcolorbox}[colback=cernBlue,colframe=black,title=Computational performance optimization,colbacktitle=cernBlue,sharp corners,boxrule=0.5pt,standard jigsaw,opacityback=0.2,text width=0.463\textwidth,boxsep=5mm,height=315mm]
    \fontsize{30}{30}\selectfont
The biggest challenge for online transverse instability
detection is to handle the high throughput. The application
needs to process a datastream at 1 Gbit/s. The first imple-
mentation was a proof of concept where the implementation
was non-multithreaded and used no special instructions or
matrix libraries. It just used standard C++ operations which
was applied to a complete data package which in this case is
a 4094$\times$3564 matrix. In that implementation the analyze of
4096 turns took 6 seconds. A simple multi threaded library
called ParallelFir was implemented which did the analyze in
the same way but the computation was split up by bunches
so each thread worked on a set of bunches. This sped up the
computation time to 1.8 seconds.
By optimizing ParallelFir using SSE4.1 instructions the
computation time was reduced to 0.6 seconds. To greater
improve the performance the pipeline design was introduced.
Each stage in the pipeline uses extensive use of the Advanced
Vector Extension(AVX2) instruction set introduced by Intel
in their Haswell microarchitecture. It uses 256 bit registers
specifically for SIMD instructions. The splicing of the data
into separate turns reduced cache misses since the data is
always read consecutive. All this combined dramatically
reduced the computation time to 80ms.
\begin{minipage}[t]{0.95\textwidth}
    \vspace*{0pt}
    \centering
\begin{tikzpicture}
\begin{axis}[
    title={Reduction of computation time},
        x label style={at={(axis description cs:0.5,0.0)},anchor=north},
    y label style={at={(axis description cs:0.0,.5)},anchor=south},
    xlabel={Iteration},
    ylabel={Processing time},
    xmin=1, xmax=4,
    ymin=0, ymax=6,
    xtick={1,2,3,4},
    ytick={0.08,0.6,1.8,6},
    legend pos=north west,
    ymajorgrids=true,
    grid style=dashed,height=100mm,
width=0.9\textwidth,
]
 
\addplot[
    color=blue,
    mark=dot,
    ]
    coordinates {
    (1,6)(2,1.8)(3,0.6)(4,0.08)
    };
\end{axis}
%\node[below] at (0.3,1.95) {\textcolor{red}{1}};%
%\node[below] at (1.4,0.9) {\textcolor{red}{2}};%
%\node[below] at (2.6,0.5) {\textcolor{red}{3}};%
%\node[below] at (3.65,0.35) {\textcolor{red}{4}};%
%\node[right] at (2.3,2.0) {\makecell[l]{$2^{nd}$ iteration using naive \\parallel implementation}};%
%\node[right] at (4.1,1.0) {\makecell[l]{$3^{rd}$ iteration,\\optimization using SSE4.1}};%
%\node[right] at (4.6,-0.6) {\makecell[l]{$4^{th}$ iteration with complete\\ redesign using a pipeline\\ approach.\textcolor{red}{80ms}}};%
\end{tikzpicture}

%\makecell[l]{$4^{th}$ iteration using standard C++ \\operations and no multi threading}
\end{minipage}
\end{tcolorbox}
\vspace*{8mm}
%conclusion
\begin{tcolorbox}[colback=cernBlue,colframe=black,title=Conclusion,colbacktitle=cernBlue,sharp corners,boxrule=0.5pt,standard jigsaw,opacityback=0.2,text width=0.463\textwidth,boxsep=5mm]
\fontsize{30}{30}\selectfont
It was demonstrated, that the implementation can handle the full data throughput from the ADTObsBoxes 5 times fast than required, providing a true online, bunch by bunch transverse instability detection system for the LHC accelerator. The detection algorithm was tuned using real data samples from the machine and will be put into operation after the winter technical stop 2016/2017. The detection system sends a trigger to all observation systems connected to the LIST along the LHC machine in case a growing transverse activity is detected, together with detailed information about which bunches are unstable, at the moment the instability is detected. This greatly improves the quality and turnaround of instability analysis, for example during scrubbing, or other special runs.
\end{tcolorbox}
\end{multicols}
\end{document}