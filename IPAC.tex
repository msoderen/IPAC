\documentclass[a4paper,
               %boxit,
               %titlepage,   % separate title page
               %refpage      % separate references
              ]{jacow}

\makeatletter%                           % test for XeTeX where the sequence is by default eps-> pdf, jpg, png, pdf, ...
\ifboolexpr{bool{xetex}}                 % and the JACoW template provides JACpic2v3.eps and JACpic2v3.jpg which might generates errors
 {\renewcommand{\Gin@extensions}{.pdf,%
                    .png,.jpg,.bmp,.pict,.tif,.psd,.mac,.sga,.tga,.gif,%
                    .eps,.ps,%
                    }}{}
\makeatother

\ifboolexpr{bool{xetex} or bool{luatex}} % test for XeTeX/LuaTeX
 {}                                      % input encoding is utf8 by default
 {\usepackage[utf8]{inputenc}}           % switch to utf8

\usepackage[USenglish]{babel}
\usepackage{multirow}
\usepackage[font=small,labelfont=bf,tableposition=top]{caption}
\usepackage{blindtext}
\usepackage{cuted}
\usepackage{float} 
\usepackage[]{algorithm2e}
\usepackage{multicol,caption}
\usepackage{tikz}
\usepackage{anyfontsize}
\newenvironment{Figure}
  {\par\medskip\noindent\minipage{\linewidth}}
  {\endminipage\par\medskip}

\ifboolexpr{bool{jacowbiblatex}}%        % if BibLaTeX is used
 {%
  \addbibresource{jacow-test.bib}
  \addbibresource{biblatex-examples.bib}
 }{}

\newcommand\SEC[1]{\textbf{\uppercase{#1}}}

%%
%%   Lengths for the spaces in the title
%%   \setlength\titleblockstartskip{..}  %before title, default 3pt
%%   \setlength\titleblockmiddleskip{..} %between title + author, default 1em
%%   \setlength\titleblockendskip{..}    %afterauthor, default 1em

%\copyrightspace %default 1cm. arbitrary size with e.g. \copyrightspace[2cm]

% testing to fill the copyright space
%\usepackage{eso-pic}
%\AddToShipoutPictureFG*{\AtTextLowerLeft{\textcolor{red}{COPYRIGHTSPACE}}}

\begin{document}


\title{Online bunch by bunch transverse instability detection in LHC}

\author{G.~Kotzian,  M.~Ojeda-Sandonís, M.E.~S\"{o}der\'{e}n\thanks{martin.soderen@cern.ch}, D.~Valuch, CERN, Geneva, Switzerland,}

\maketitle
\begin{multicols}{2}
%
\begin{abstract}

Reliable detection of developing transverse instabilities in the Large Hadron Collider is one of the main operational challenges of the LHC's high intensity proton run. A full machine snapshot provided from the moment of instability is a crucial input to develop and fine tune instability models. The transverse feedback system (ADT) is the only instrument in LHC, where a full rate bunch by bunch transverse position information is available. Together with a sub-micron resolution it makes it a perfect place to detect transverse beam motion. Very large amounts of data, at very high data rates (8 Gb/s) need to be processed on the fly to detect onset of transverse instability. A very powerful computer system (so called ADTObsBox) was developed and put into operation by the CERN RF group, which is capable of processing the full rate data streams from ADT and perform an on the fly instability detection. The output of this system is a timing event with a list of all bunches developing instability, which is then sent to the LHC-wide instability trigger network to freeze other observation instruments. The device also provides buffers with raw position data for offline analysis.
\end{abstract}

\section{Introduction}
Since the commission of the ADTObsBox in 2015 it has been used for injection oscillation transient drift observation and been a valuable resource during several machine development runs. The bunch-by-bunch positional data have been available through subscription to the ObsBoxBuffer FESA class\cite{fesa}. An online transverse instability detection system has been designed which not only detects an instability in the LHC but also which bunch and over which turns. By sending a trigger through the LIST network with this information a snapshot of the LHC can be captured and the cause of the instability can be analyzed \cite{LIST}.
\section{Detection}
The system needs to detect a wide variety of instabilities with rise times from a couple of hundreds of turns up to tens of thousands and at the same time produce as few false triggers as possible. Moving averages have been tested in the LHC BBQ\cite{instabilityMonitoring} but it was not best suited for the BBQ because of its high noise floor. However the noise floor is significantly lower in the ADTObsBox.  It is also fast and simple and a good start for exploring the possibilities for online transverse instability detection.
\subsection{Algorithm}
Before the actual detection the amplitude of the oscillation is calculated and used as input to the detection algorithm.
By using multiple moving averages with different lengths different rise times can be detected. It compares the average of $W$ samples against a threshold calculated from earlier samples where $W$ is the window length. The sensitivity can be adjusted by setting how much higher the new average must be above the threshold and longer rise times can be detected by limiting the growth rate of the threshold. When a instability is detected a trigger is sent to the LIST network informing which bunch,which turns and potentially also the rise time so other devices can capture a snapshot of the LHC for analyzing the cause of the instability. By adding this extra information time spent on analyzing the cause can be reduced.
\begin{algorithm}[H]
 \KwData{Oscillation Amplitude($A[n]$)}
 \KwResult{instability trigger}
 \tcc{Prefill $avg$ and $avg_{old}$ with samples}
 $avg=\sum\limits_{n=0}\limits^{W}\dfrac{A[n]}{W}$\;
 $thres=avg$\;
 \tcc{Do this for every amplitude sample}
 \While{forever}{
 \For{W times}{
 \tcc{update average and remove the oldest sample}
 $avg+=new \; \dfrac{A}{W}$\;
 $avg-=oldest \;\dfrac{A}{W}$\;
 }
 \tcc{$\omega$ adjusts the sensitivity}
   \If{$thres<\omega\times avg$}{
   send trigger\;
   }
   \tcc{$\beta$ can be set to limit the growth rate of the threshold}
   $thres$=$\beta\times avg+(1-\beta)\times thres$\;
 }
 \caption{Simplified algorithm}
\end{algorithm}
During normal operation it is expected that $thres\approx avg$ holds and during an exponential amplitude growth $thres<avg$ holds.
\subsection{Analytic signal calculation}
The first step is to calculate the amplitude of the oscillation. The instantaneous amplitude is calculated using the Hilbert transforms \cite{alan1989discrete}. This method is already used in the ADT for analyzing damper performance\cite{extraction}.
\begin{enumerate}
\item The data is sliced turn-by-turn, converted to float and memory-align with 32 bit for AVX2 compability\cite{AVX}. See {\color[rgb]{1,0,0} 1} in figure~\ref{fig:ADTOverview}.
\item To remove the static offset from the positional data a simple notch filter is used. See {\color[rgb]{1,0,0} 2} in figure~\ref{fig:ADTOverview}
\item The I and Q component of the signal is calculated using the Hilbert transform with a kaiser window of length 7. See {\color[rgb]{1,0,0} 3} in figure~\ref{fig:ADTOverview}
\item The amplitude is calculated from the I and Q components. See {\color[rgb]{1,0,0} 4} in figure~\ref{fig:ADTOverview}
\end{enumerate}
\begin{minipage}{\columnwidth}
   \resizebox {\columnwidth} {!} {
   \fontsize{20}{10}
\input{pipeline.tex}
}
\captionof{figure}{An overview of the LHC online instability detection pipeline.}
\label{fig:ADTOverview}
\end{minipage}


\subsection{Tuning and testing}
Observation data from the ObsBox with transverse instabilities has been stored from the LHC operations during 2016. Tuning was automated by first manually extracing interesting data set and adding meta data about the instability. A test environment was set up and the pipeline was tested with this data and many combinations of configurations to find the maximum coverage of instabilities and as few false triggers as possible. Example output from the algorithm can be seen in figure~\ref{fig:filters}.
It has also been tested by simulating a beam in SR4 using a signal generator which feeds a signal directly into the ADT DSPUs and consequently the ObsBoxes.
\section{Implementation}
The ObsBoxes are four off the shelf SuperMicro 6028U-TR4+ servers which are capable of concurrent online data analysis\cite{highBandwidthProcessing}.
On the servers there are several instances of the ObsBoxBuffer FESA class which allows for data acquisition of different number of turns. There are specific buffers reserved for online analysis. A FESA class called ALLADTCopra has been developed which subscribes to these buffers and it receives the data over a Gigabit link at close to full rate and performs the online instability analysis.

\end{multicols}

\begin{figure*}[!tbh]
\includegraphics[ width=\textwidth]{plot.png}
     \caption{Output from instability detection with data from observations}
     \label{fig:filters}
\end{figure*}
\begin{multicols}{2}

\subsection{Handling the high throughput}
The biggest challenge for online transverse instability detection is to handle the high throughput. The application needs to process a datastream at 1 Gbit/s. The first implementation was a proof of concept where the implementation was non-multithreaded and used no special instructions or matrix libraries. It just used standard C++ operations which was applied to a complete data package which in this case is a 4094$\times$3564 matrix. In that implementation the analyze of 4096 turns took 6 seconds. A simple multi threaded library called ParallelFir was implemented which did the analyze in the same way but the computation was split up by bunches so each thread worked on a set of bunches. This sped up the computation time to 1.8 seconds. 
\\
\\ 
By optimizing ParallelFir using SSE4.1 instructions the computation time was reduced to 0.6 seconds. To greater improve the performance the pipeline design was introduced. Each stage in the pipeline uses extensive use of the Advanced Vector Extension(AVX2) instruction set introduced by Intel in their Haswell microarchitecture. It uses 256 bit registers specifically for SIMD instructions. The splicing of the data into separate turns reduced cache misses since the data is always read consecutive. All this combined dramatically reduced the computation time to 80ms.
\subsection{Performance testing}
The performance of the implementation was compared with a native C++ implementation. Compiled with G++-4.4.7 since that is the standard compiler for front-end computers at CERN. Both were compiled with maximum compiler optimization enabled so the native implementation used auto vectorization. For the conversion stage data for 131072 turns and 3564 bunches were simulated. For the other stages data for one turn and 128000000 bunches were simulated.
\newline
\vspace*{1 mm}
\newline
\begin{tabular}{l*{6}{c}r}
Conversion              & Instructions & Cycles & CPU time[ms] \\
\hline
AVX & 793964717 & 279384376 &  151 \\
Native            & 1534437270 & 714719743 & 285  \\
\hline
Reduction	&48.3\% &60.9\% &47.0\%
\end{tabular}
\newline
\vspace*{2 mm}
\newline
\begin{tabular}{l*{6}{c}r}
Notch              & Instructions & Cycles & CPU time[ms] \\
\hline
AVX & 145416604 & 143719902 &  100 \\
Native            & 1430236599 & 493511370 & 268  \\
\hline
Reduction	&89.8\% &70.9\% &62.6\%
\end{tabular}
\newline
\vspace*{2 mm}
\newline
\begin{tabular}{l*{6}{c}r}
IQ              & Instructions & Cycles & CPU time[ms] \\
\hline
AVX & 2003146566 & 1233847315 &  515 \\
Native            & 4158509240 & 1361599075 & 559  \\
\hline
Reduction	&51.8\% &9.4\% &7.9\%
\end{tabular}
\newline
\vspace*{2 mm}
\newline
\begin{tabular}{l*{6}{c}r}
Amplitude              & Instructions & Cycles & CPU time[ms] \\
\hline
AVX & 225351692 & 185194294 &  128 \\
Native            & 2845177110 & 4487822502 & 1692  \\
\hline
Reduction	&92.1\% &95.9\% &92.4\%
\end{tabular}
\newline
\vspace*{0.5 mm}
\newline
The conversion, notch and amplitude stages show significant decrease in computation time. The IQ stage does not show much improvement and a probable cause is because the I is only a delay of three turns and this is implemented using a memcpy where the data is copied from one structure to another.
\\
\\
\section{Results}
The implementation can handle the throughput from the ObsBoxes and it has been tuned with the data which we have available. The result is available through a FESA class and a fixed display for the CCC has also been developed. 
\section{Future Work}
The LHC instability detection will be tested during the startup in May and we expect to see instabilities during the commissioning. 

\begin{thebibliography}{99}

\bibitem{instabilityMonitoring} T. Levens, K. Lasocha, T. Lefevre, 'Recent developments for instability monitoring at the LHC', Proceedings of IBIC16, Barcelona, Spain, (2016)

\bibitem{fesa} M. Arruat, L. Fernandez, S. Jackson, F. Locci, J. Nougaret, 
M. Peryt, A. Radeva, 
M. Sobczak, M. Eynden  'FRONT-END SOFTWARE ARCHITECTURE ',  ICALEPCS, Knoxville, Tennessee, USA, (2007)

\bibitem{AVX} "How Intel® AVX2 Improves Performance on Server Applications", website: https://software.intel.com/en-us/articles/how-intel-avx2-improves-performance-on-server-applications

\bibitem{highBandwidthProcessing} M. Ojeda, P. Baudrenghien, A. Butterworth, J. Galindo, W. Hofle, T.E Levens, J.C Molendijk, D. Valuch, 'Processing high-bandwidth bunch-per-bunch observation data from the rf and transverse damper system of the LHC',  ICALEPCS, Melbourne, Australia, (2015)

\bibitem{alan1989discrete} A. Oppenheim, W. Ronald, and R. John, 'Discrete hilbert transforms', in Discrete-time signal processing,  (1989)

\bibitem{extraction} G. Kotzian, ' 	Transverse Feedback Parameter Extraction from Excitation Data',  IPAC, Copenhagen, Denmark, (2017)

\bibitem{LIST} T. Włostowski, G. Daniluk, M. Lipinski, J. Serrano, F. Vaga, 'Trigger and RF Distribution Using White Rabbit',  ICALEPCS, Melbourne, Australia, (2015)


\end{thebibliography}
\iftrue   % balancing with bad results
	\newpage
	\raggedend
\fi




%\section{appendix}
%Any appendix should be in a separate section directly preceding
%the \SEC{References} section. If there is no \SEC{References} section,
%this should be the last section of the paper.
\end{multicols}

\end{document}
